## Overview

Repository for testing how to build Windows container images with GitLab CI/CD and Runner

## Reference

- [Tutorial: Containerize a .NET app](https://learn.microsoft.com/en-us/dotnet/core/docker/build-container?tabs=windows&pivots=dotnet-8-0)